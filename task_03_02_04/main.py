# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_03_02_04.
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!


n = int(input())
a = n // 100
b = n // 10 % 10
c = n % 10
print(a + b + c)
