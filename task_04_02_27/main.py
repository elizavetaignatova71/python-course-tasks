# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_27.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


c = 2 
count = 0
n = int(input("n = "))
while (count < n):
    if all (c % j !=0 for j in range(2,c)):
        print(c,end=" ")
        count+=1

    c+=1
