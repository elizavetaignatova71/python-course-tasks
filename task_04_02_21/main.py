# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_21.
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!


lst = [-170.4, -174.5, 167.4, 161.3]
boy = 0
count_boy =  0
girl = 0
count_girl = 0

for l in lst:
    if l < 0:
       boy += 1
       count_boy += 1
    else:
        girl += 1
	    count_girl += 1
print(boy / count_boy, girl / count_girl)
