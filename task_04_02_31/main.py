# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_31.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


s = input("Введите предложение = ") 
k = input("Введите букву = ")
words = s.split()
for word in words:
    if k in word.lower():        
        words.remove(word)    
    else:        
        print(word, end=' ')
