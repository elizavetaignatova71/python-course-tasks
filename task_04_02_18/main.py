# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_18.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


sentence = "програмирование"
vowels = 0
consonants = 0
for i in sentence:
    letter = i.lower()
    if letter == "a" or letter == "e" or\
       letter == "i" or letter == "o" or\
       letter == "u" or letter == "y":
        vowels += 1
    else:
        consonants += 1
print("Vowels %i\nConsonants %i" % (vowels, consonants))
