# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_30.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!



nums = [int(item) for item in input().split()]


for item in nums:
    if item <= 0:
        all_pos_1 = False
        break
else:
    all_pos_1 = True

all_pos_2 = all([item > 0 for item in nums])

for item in nums:
    if item == 0:
        any_zero_1 = True
        break
else:
    any_zero_1 = False

any_zero_2 = any([item == 0 for item in nums])

def even_or_odd(a):
    if a % 2 == 0:
        print('Четное число')
    else:
        print('Нечентное число')


    print("Odd" if int(input("Number: ")) & 1 else "Even")



print("Все положительные:", all_pos_1, all_pos_2)
print("Хотя бы 1 нулевой элемент:", any_zero_1, any_zero_2)
print("Все четные")
print("Хотя бы 1 нечетный элемент")

