# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_05_01. Вариант !!!
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!




db = [
    {
        "name": "Игнатова Елзавета",
        "birthday": "17/08/2004",
        "height": 170,
        "weight": 50,
        "car": True,
        "languages": ["С++", "Python"]
    },
    
        ...
]

while True:
    print("\n-----")
    print("Меню")
    print("-----")
    print("1. Список сотрудников.")
    print("2. Фильтр по языку программирования.")
    print("3. Средний рост сотрудников, моложе указанного г.р.")
    print("\nВыберите пункт меню или нажмите ENTER для выхода: ", end="")

    answer = input()

    if answer == "1":
        print("Содержимое базы данных ({}):".format(len(db)))
       
        for i, item in enumerate(db, start=1):
            print("{}.".format(i))
            print("Имя: {}".format(item["name"]))

    elif answer == "2":
        lang = input("Введите язык программирования: ")
        lang = lang.capitalize()
        res = []
        for item in db:
            if lang in item["languages"]:                 res.append(item)
        if len(res) > 0:
            print("Список сотрудников со знанием "
                   "языка программирования {} ({}):".format(lang, len(res)))
            for i, item in enumerate(res, start=1):
                print("{}.".format(i))
                print("Имя: {}".format(item["name"]))
 
        else:
            print("Таких соотрудников нет.")
    elif answer == "3":
        younger_than = int(input("Введите год рождения сотрудника: "))
 
        r_sum = 0
        r_col = 0
        for item in db:
            year = int(item["birthday"][-4:])
            if year >= younger_than:
                r_sum += item["height"]
                r_col += 1
 
        if r_col > 0:
            print("Средний рост сотрудников, {} г.р. и моложе: ({:.1f}) см.".
                  format(younger_than, r_sum / r_col))
        else:
            print("Таких сотрудников нет.")
 
    else:
        break


