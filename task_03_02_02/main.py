# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_03_02_02.
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!


x = int(input("x="))
y = int(input("y="))
z = int(input("z="))
res = (((((x**5) + 7) / abs (-6) * y)) ** (1 / 3)) / (7 - z % y)

print("", round(res, 3))
