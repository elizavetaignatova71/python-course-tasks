# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_22.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


from    random  import randint

N=5 

nums = [randint(1, 20) for i in range(N)]

print (nums)

a_max = nums[0]
a_min = nums[0]

for i in range(N):
    if nums[i] > a_max : a_max=nums[i]
    if nums[i] < a_min : a_min=nums[i]

print ('max:', a_max, 'min:', a_min)
