# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_26.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


n = int(input())
d = 0
for i in range(1, int(n)+1):
    for j in range(1, i+1):
        if i % j == 0:
            d += 1
    print(i, d * "*")
    d = 0
