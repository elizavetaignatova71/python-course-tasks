# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_15.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


p = float(input("Введите грузоподъемность: "))
n = int(input("Введите количество предметов, а затем массу каждого предмета: "))  
subjects = [float(input()) for i in range(n)]
print("Итоговая масса груза: {}".format(sum(subjects)))
if sum(subjects) <= p:
    print("Перевозка груза возможна")
else:
    print("Превышена грузоподъемность")
