# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_07.
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!


nums_sum = 0  # сумма
nums_count = 0  # количество

x, n, summ = 1, -1, -1
while x != 0:
  summ += x
  n += 1
  x = int(input())
print(summ, n)

