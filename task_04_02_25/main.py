# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_25.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


n = int(input())
for i in range(i,n + 1):
   print(sep = "/n")
   for j in range(1, n + 1):
       print(i,"*", j, "=", j*i, end = " ")
