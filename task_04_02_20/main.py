# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_20.
#
# Выполнил: Игнатова Е. Е.
# Группа: !!!
# E-mail: !!!


n = int(input("n="))
import time
start_time = time.time()
for i in range(1000, 2000):
   i1 = i // 100
   i2 = i // 100 % 10
   i3 = i // 10 % 10
   i4 = i % 10
   if i1+i2+i3+i4 == n:
       print(i, end=' ')
 
print("--- %s sekonds ---" (time. time() - start_time)
