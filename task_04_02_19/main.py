# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_19.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


a = int(input("a = "))
b = int(input("b = "))
c = int(input("c = "))
count = 0
while (count < a,b): 
    if all (a % j !=0 for j in range(a,b)):
            print(c,end=" ")
            count+=1
    c+=1
# --------------
# Пример вывода:
#
# a = 1
# b = 10
# c = 2
# 2 4 6 8 10
