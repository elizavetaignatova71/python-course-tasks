# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_14.
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!


s = int(input())
p = int(input())
i = 2
total = s
while i <= 10:
    s = s + s/100*p
    print("Пробег за {}-й день: {:.1f} км.".format(i,s))
    total += s
    i += 1
    

print("Суммарный пробег: {:.1f} км.".format(total))


