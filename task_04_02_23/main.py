# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_23.
#
# Выполнил: Игнатова Е.Е.
# Группа: ИС
# E-mail: !!!


n = 8
lst = [1,1]
while lst[-1] <= n:
    lst.append(lst[-2] + lst[-1])
    if lst [-1] == n:
        print("True")
        break
print(lst)
