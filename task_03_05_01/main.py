# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_03_05_01. Вариант !!!
#
# Выполнил: Игнатова Е. Е.
# Группа: ИС
# E-mail: !!!




db = [
    {
        "name": "Игнатова Елизавета",
        "birthday": "17/08/2004",
        "height": 170,
        "weight": 50,
        "car": True,
        "languages": ["Python"]
    }
    

]
print("Содержимое базы данных ({}):".format(len(db)))
 
print("1.")
 
print("Имя: {}".format(db[0]["name"]))
print("День рождения: {}".format(db[0]["birthday"]))
print("Рост (см.): {}".format(db[0]["height"]))
print("Вес (кг.): {}".format(db[0]["weight"]))
print("Есть машина: {}".format(db[0]["car"]))
print("Языки программирования: {}".format(db[0]["languages"]))


